import com.edocsol.delivery.Courier;
import com.edocsol.delivery.Role;
import com.edocsol.delivery.UserRole;
import com.edocsol.delivery.User;

class BootStrap {

    def init = { servletContext ->
		
		Courier UPS = new Courier(name:'UPS').save()
		Courier FedEx = new Courier(name:'FedEx').save()
		Courier USPS = new Courier(name:'US Mail').save(flush:true)
		
		User stephen = new User (username:'Stephen',password: '.').save()
		Role user = new Role(authority: 'ROLE_USER').save()
		new UserRole(user: stephen, role: user).save(flush:true)
		
		User ryan = new User (username:'Ryan',password: '.').save()
		Role admin = new Role(authority: 'ROLE_ADMIN').save()
		new UserRole(user: ryan, role: admin).save()
		new UserRole(user: ryan, role: user).save(flush:true)
    }
    def destroy = {
    }
}
