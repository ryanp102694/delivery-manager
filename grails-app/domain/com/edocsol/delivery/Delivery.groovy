package com.edocsol.delivery

class Delivery {
	
	Date receivedAt = new Date()
	int pieceCount
	String notes
	Courier courier
	static belongsTo = Courier
	
    static constraints = {
		courier nullable:false 
		receivedAt nullable:false 
		pieceCount min:0 
		notes nullable:true 
    }
}
